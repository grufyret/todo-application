<?php

class TodoListCest
{
    // tests
    public function iShouldGetTasks(ApiTester $I)
    {
        $I->sendGET('/');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);

    }

    public function iShouldCreateTask(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/', [
            'task_title' => 'Task 9',
            'task_name' => 'Codeception test success and fail',
            'status' => 'Inprogress',
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();

        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeResponseContainsJson(['data' => ['task_title' => 'Task 9']]);
 
   }

   public function iShouldUpdateTask(ApiTester $I)
   {
       $I->haveHttpHeader('Content-Type', 'application/json');
       $I->sendPOST('/', [
           'task_title' => 'TaskToUpdate',
           'task_name' => 'Codeception test update',
           'status' => 'Inprogress',
       ]);

       $response = json_decode($I->grabResponse(), true);
       $taskId = $response['data']['id'];

       $I->haveHttpHeader('Content-Type', 'application/json');
       $I->sendPUT("/update/{$taskId}", [
           'task_title' => 'Updated Task',
           'task_name' => 'Codeception test update success',
           'status' => 'Completed',
       ]);

       $I->seeResponseCodeIs(200);
       $I->seeResponseIsJson();
       $I->seeResponseContainsJson(['status' => 'success']);
       $I->seeResponseContainsJson(['data' => ['task_title' => 'Updated Task']]);
   }

   public function iShouldDeleteTask(ApiTester $I)
    {
        // Send a POST request to create a task first
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/', [
            'task_title' => 'TaskToDelete',
            'task_name' => 'Codeception test delete',
            'status' => 'Inprogress',
        ]);

        // Get the created task ID from the response
        $response = json_decode($I->grabResponse(), true);
        $taskId = $response['data']['id'];

        // Send a DELETE request to delete the task
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDELETE("/delete/{$taskId}", ['action' => 'confirm']);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeResponseContainsJson(['data' => ['id' => $taskId]]);
    }

    

   
    
//fail test
    public function iShouldFailCreateTaskMissingTitle(ApiTester $I)
   {
       $I->haveHttpHeader('Content-Type', 'application/json');
       $I->sendPOST('/', [
           'task_name' => 'Codeception test success and fail',
           'status' => 'Inprogress',
       ]);

       $I->seeResponseCodeIs(200);
       $I->seeResponseIsJson();
       $I->seeResponseContainsJson(['status' => 'failed']);
   }

   public function iShouldFailUpdateTaskMissingFields(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('/', [
            'task_title' => 'TaskToUpdate',
            'task_name' => 'Codeception test update',
            'status' => 'Inprogress',
        ]);

        $response = json_decode($I->grabResponse(), true);
        $taskId = $response['data']['id'];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPUT("/update/{$taskId}", [
       
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'failed']);
        $I->seeResponseContainsJson(['message' => 'No fields provided for update']);
    }

    public function iShouldFailDeleteTaskInvalidId(ApiTester $I)
    {
        // Send a DELETE request with an invalid ID
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDELETE("/delete/invalid_id", ['action' => 'confirm']);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'failed']);
        $I->seeResponseContainsJson(['message' => 'Invalid ID']);
    }

}


