<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");

require_once('MysqliDb.php');

class API {
    private $db;

    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'employee');
    }

    private function respondError($method, $message)
    {
        echo json_encode([
            'method' => $method,
            'status' => 'failed',
            'message' => $message,
        ]);
    }

    private function respondSuccess($method, $data)
    {
        echo json_encode([
            'method' => $method,
            'status' => 'success',
            'data' => $data,
        ]);
    }

    public function httpGet($payload)
    {
        $tasks = $this->db->get('tbl_to_do_list');

        if ($tasks) {
            $this->respondSuccess('GET', $tasks);
        } else {
            $this->respondError('GET', 'Failed to retrieve tasks');
        }
    }

    public function httpPost($payload)
    {
        $task_title = $payload['task_title'] ?? '';
        $task_name = $payload['task_name'] ?? '';

        if (empty($task_title) || empty($task_name)) {
            $this->respondError('POST', 'Missing required fields');
            return;
        }

        $data = [
            "task_title" => $task_title,
            "task_name" => $task_name,
            "time" => date("Y-m-d H:i:s"),
            "status" => "Inprogress",
        ];

        $id = $this->db->insert("tbl_to_do_list", $data);

        if ($id) {
            $this->respondSuccess('POST', [
                "id" => $id,
                "response"=>$data
            ]);
        } else {
            $this->respondError('POST', 'Failed to create a new task');
        }
    }

    public function httpPut($id, $payload)
    {
        if (isset($payload['id'])) {
            $id = $payload['id'];
        }

        $task_title = $payload['task_title'] ?? '';
        $task_name = $payload['task_name'] ?? '';
        $status = $payload['status'] ?? '';

        if (empty($task_title) && empty($task_name) && empty($status)) {
            $this->respondError('PUT', 'No fields provided for update');
            return;
        }

        $data = [];
        if (!empty($task_title)) {
            $data["task_title"] = $task_title;
        }
        if (!empty($task_name)) {
            $data["task_name"] = $task_name;
        }
        if (!empty($status)) {
            $data["status"] = $status;
        }

        $this->db->where("id", $id);
        $result = $this->db->update("tbl_to_do_list", $data);

        if ($result) {
            $updatedRecord = $this->db->where("id", $id)->getOne("tbl_to_do_list");

            if ($updatedRecord) {
                $this->respondSuccess('PUT', $updatedRecord);
            } else {
                $this->respondError('PUT', 'Failed to retrieve updated record');
            }
        } else {
            $this->respondError('PUT', 'Failed to update the task');
        }
    }

    public function httpDelete($id, $payload)
    {
        if (isset($payload['id'])) {
            $id = $payload['id'];
        }

        if (!isset($payload['action']) || $payload['action'] !== 'confirm') {
            $this->respondError('DELETE', "Missing or invalid 'action' field in payload. Use 'confirm' to delete.");
            return;
        }

        if (!is_numeric($id) || $id <= 0) {
            $this->respondError('DELETE', 'Invalid ID');
            return;
        }

        $this->db->where("id", $id);
        $result = $this->db->delete("tbl_to_do_list");

        if ($result) {
            $this->respondSuccess('DELETE', ['id' => $id]);
        } else {
            $this->respondError('DELETE', 'Failed to delete record');
        }
    }
}

$request_method = $_SERVER['REQUEST_METHOD'];

if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];

        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));
        $last_index = count($exploded_request_uri) - 1;
        $ids = $exploded_request_uri[$last_index];
    }
}

$received_data = json_decode(file_get_contents('php://input'), true);

$api = new API();

switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}
